import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Feedback from '@/components/Feedback'
import Register from '@/components/Register'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'vuetify/dist/vuetify.min.js' // Ensure you are using css-loader

import auth from '@/middleware/auth.js'

Vue.use(Router)
Vue.use(Vuetify)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: auth.redirectIfNotAuthenticated
    },
    {
      path: '/feedback',
      name: 'Feedback',
      component: Feedback,
      beforeEnter: auth.redirectIfNotAuthenticated
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: auth.redirectIfAuthenticated
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      beforeEnter: auth.redirectIfAuthenticated
    },
    {
      path: '/Logout',
      name: 'Logout',
      beforeEnter: (to, from, next) => {
        localStorage.removeItem('jwt')
        next({name: 'Login'})
      }
    }
  ],
  mode: 'history'
})

export default router
