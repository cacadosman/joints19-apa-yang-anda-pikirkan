import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    pageTitle: 'Page Not Found'
  },
  mutations: {
    changePageTitle (state, title) {
      state.pageTitle = title
    }
  },
  getters: {
    getPageTitle: state => {
      return state.pageTitle
    }
  }
})

export default store
