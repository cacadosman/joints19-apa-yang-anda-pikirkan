<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/api/auth', 'AuthController@authenticate');
$router->post('/api/register', 'AuthController@register');
$router->group(
    array(
        "middleware" => "jwt.auth"
    ),
    function() use($router){
        $router->get('/api/feeds', 'FeedController@index');
        $router->post('/api/feeds', 'FeedController@store');
        $router->post('/api/feedbacks', 'FeedbackController@store');
        $router->post('/winnerwinnerchickendinner', 'FlagController@win');
    }
);