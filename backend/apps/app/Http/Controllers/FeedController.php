<?php
    namespace App\Http\Controllers;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;

    class FeedController extends Controller
    {
        private $request;

        public function __construct(Request $request)
        {
            $this->request = $request;
        }

        public function index()
        {
            $data = DB::table("feeds")
                        ->join('users', 'feeds.user_id', '=', 'users.id')
                        ->select('feeds.id', 'users.username', 'feeds.content')
                        ->get();

            return response()->json(array(
                "data" => $data,
                "status" => true
            ), 200);
        }

        public function store()
        {
            $this->validate($this->request, array(
                'content'  => 'required'
            ));

            DB::table("feeds")->insert(array(
                "user_id" => $this->request->auth->id,
                "content" => $this->request->content
            ));

            return response()->json(array(
                "status" => true
            ), 200);
        }
    }