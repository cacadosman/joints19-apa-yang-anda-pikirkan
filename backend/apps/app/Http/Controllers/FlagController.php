<?php
    namespace App\Http\Controllers;

    use Illuminate\Http\Request;

    class FlagController extends Controller
    {
        private $request;
        private $flag;

        public function __construct(Request $request)
        {
            $this->request = $request;
            $this->flag = "ASGama{om3det0u_y0u_g0t_m3}";
        }

        public function win()
        {
            if($this->request->auth->is_admin)
            {
                return response()->json(array(
                    "flag" => $this->flag,
                    "status" => true
                ), 200);
            }

            return response()->json(array(
                "flag" => "ea gan belom beruntung :v",
                "status" => false
            ), 403);
        }
    }