<?php
    namespace App\Http\Controllers;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;
    use Firebase\JWT\JWT;
    use Firebase\JWT\ExpiredException;
    use Illuminate\Support\Facades\Hash;

    class AuthController extends Controller
    {
        private $request;
        private $redis;

        public function __construct(Request $request)
        {
            $this->request = $request;
            $this->redis = app("redis");
        }

        protected function jwt(object $user)
        {
            $data = array(
                "id" => $user->id,
                "username" => $user->username,
                "is_admin" => $user->is_admin
            );

            $payload = [
                'iss' => env('JWT_ISSUER'),
                'sub' => $data,
                'iat' => time(),
                'exp' => time() + 60*60
            ];
            
            return JWT::encode($payload, env('JWT_SECRET'));
        }

        public function authenticate()
        {
            $this->validate($this->request, array(
                'username'     => 'required',
                'password'  => 'required'
            ));

            $username = $this->request->username;

            if($this->redis->exists($username))
            {
                $user = json_decode($this->redis->get($username));
            }
            else
            {
                $user = DB::table('users')
                        ->where('username', $username)
                        ->first();
            }

            if ($user && $this->request->input('password') === $user->password) {
                $this->redis->set($username, json_encode((array) $user));
                return response()->json(array(
                    'token' => $this->jwt($user)
                ), 200);
            }

            return response()->json(array(
                "status" => false,
                "message" => "Username/Password Salah"
            ), 401);
        }

        public function register()
        {
            $this->validate($this->request, array(
                "username" => "required|unique:users",
                "password" => "required"
            ));

            DB::table("users")->insert(array(
                "username" => $this->request->username,
                "password" => $this->request->password
            ));

            return response()->json(array(
                "status" => true
            ), 200);
        }
    }