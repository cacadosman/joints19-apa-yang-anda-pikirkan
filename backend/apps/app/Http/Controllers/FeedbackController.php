<?php
    namespace App\Http\Controllers;

    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;

    class FeedbackController extends Controller
    {
        private $request;
        private $redis;

        public function __construct(Request $request)
        {
            $this->request = $request;
            $this->redis = app("redis");
        }

        public function store()
        {
            $this->validate($this->request, array(
                'title'     => 'required',
                'content'  => 'required'
            ));

            DB::table("feedbacks")->insert(array(
                "title" => $this->request->title,
                "content" => $this->request->content
            ));
            $this->redis->set($this->request->title, $this->request->content);

            return response()->json(array(
                "status" => true
            ), 200);
        }
    }